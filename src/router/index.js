import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import CategoryProducts from '@/components/CategoryProducts'
import Checkout from '@/components/Checkout'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/category/:catName',
      name: 'CategoryProducts',
      component: CategoryProducts
    },
    {
      path: '/checkout',
      name: 'Checkout',
      component: Checkout
    }
  ]
})
